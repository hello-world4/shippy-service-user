package main

import (
	"context"
	"errors"
	"log"

	"github.com/micro/go-micro/v2"
	pb "gitlab.com/hello-world4/shippy-service-user/proto/user"
	"golang.org/x/crypto/bcrypt"
)

type authable interface {
	Decode(token string) (*CustomClaims, error)
	Encode(user *pb.User) (string, error)
}


type userService struct {
	repository Repository
	tokenService authable
	publisher micro.Publisher
}

func (s *userService) Get(ctx context.Context, req *pb.User, res *pb.Response) error {
	log.Println("Get")
	result, err := s.repository.Get(ctx, req.Id)
	if err != nil {
		return err
	}

	user := UnmarshalUser(result)
	res.User = user

	return nil
}

func (s *userService) GetAll(ctx context.Context, req *pb.Request, res *pb.Response) error {
	log.Println("GetAll")
	results, err := s.repository.GetAll(ctx)
	if err != nil {
		return err
	}

	users := UnmarshalUserCollection(results)
	res.Users = users

	return nil
}

func (s *userService) Auth(ctx context.Context, req *pb.User, res *pb.Token) error {
	log.Println("Auth")

	user, err := s.repository.GetByEmail(ctx, req.Email)
	if err != nil {
		log.Println("no user with email", req.Email)
		return err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password)); err != nil {
		log.Println("wrong password")
		return err
	}

	token, err := s.tokenService.Encode(user)
	if err != nil {
		return err
	}

	res.Token = token
	return nil
}

func (s *userService) Create(ctx context.Context, req *pb.User, res *pb.Response) error {
	log.Println("Create")

	hashedPass, err := bcrypt.GenerateFromPassword([]byte(req.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	req.Password = string(hashedPass)
	if err := s.repository.Create(ctx, marshalUser(req)); err != nil {
		return err
	}

	req.Password = ""
	res.User = req

	if err := s.publisher.Publish(ctx, req); err != nil {
		return err
	}
	log.Println("event", eventTopic, "published")

	return nil
}

func (s *userService) ValidateToken(ctx context.Context, req *pb.Token, res *pb.Token) error {
	log.Println("ValidateToken")

	claims, err := s.tokenService.Decode(req.Token)
	if err != nil {
		return err
	}

	if claims.User.Id == "" {
		return errors.New("invalid user")
	}

	res.Valid = true
	return nil
}