package main

import (
	"log"
	"time"

	"github.com/dgrijalva/jwt-go"
	pb "gitlab.com/hello-world4/shippy-service-user/proto/user"
)

var (
	salt = []byte("FGNFIOaksfjlMLkf;AKNSf;knffqqlalala_max_entropy")
)

type CustomClaims struct {
	User *pb.User
	jwt.StandardClaims
}

type TokenService struct {
	repo Repository
}

func (s *TokenService) Decode(token string) (*CustomClaims, error) {

	tokenType, err := jwt.ParseWithClaims(token, &CustomClaims{},
		func(t *jwt.Token) (interface{}, error) {
			return salt, nil
		})

	if err != nil {
		return nil, err
	}

	if claims, ok := tokenType.Claims.(*CustomClaims); ok && tokenType.Valid {
		log.Println("claims: ", claims)
		return claims, nil
	}

	return nil, err

}

func (s *TokenService) Encode(user *pb.User) (string, error) {
	claims := CustomClaims{
		user,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(24 * time.Hour).Unix(),
			Issuer:    "shippy.service.user",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString(salt)
}
