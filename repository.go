package main

import (
	"context"
	pb "gitlab.com/hello-world4/shippy-service-user/proto/user"
	"github.com/jmoiron/sqlx"
	uuid "github.com/satori/go.uuid"
	"log"
)

type User struct {
	ID string `sql:"id"`
	Name string `sql:"name"`
	Email string `sql:"email"`
	Company string `sql:"company"`
	Password string `sql:"password"`
}

type Repository interface {
	GetAll(ctx context.Context) ([]*User, error)
	Get(ctx context.Context, id string) (*User, error)
	Create(ctx context.Context, user *User) error
	GetByEmail(ctx context.Context, email string) (*pb.User, error)
}

type PostgresRepository struct {
	db *sqlx.DB
}

func NewPostgresRepository(db *sqlx.DB) *PostgresRepository {
	return &PostgresRepository{db}
}

func marshalUserCollection(users []*pb.User) []*User {
	u := make([]*User, len(users))
	for _, v := range users {
		u = append(u, marshalUser(v))
	}
	return u
}

func marshalUser(user *pb.User) *User {
	return &User{
		ID: user.Id,
		Name: user.Name,
		Email: user.Email,
		Company: user.Company,
		Password: user.Password,
	}
}

func UnmarshalUserCollection(users []*User) []*pb.User {
	u := make([]*pb.User, len(users))
	for _, val := range users {
		u = append(u, UnmarshalUser(val))
	}
	return u
}

func UnmarshalUser(user *User) *pb.User {
	return &pb.User{
		Id:       user.ID,
		Name:     user.Name,
		Email:    user.Email,
		Company:  user.Company,
		Password: user.Password,
	}
}

func (r *PostgresRepository) GetAll(ctx context.Context) ([]*User, error) {
	log.Println("GetAll")

	users := make([]*User, 0)
	err := r.db.SelectContext(ctx, &users, "SELECT * FROM users")
	if err != nil {
		return nil, err
	}
	return users, err
}

func (r *PostgresRepository) Get(ctx context.Context, id string) (*User, error) {
	log.Println("Get")

	user := &User{}
	err := r.db.GetContext(ctx, user, "SELECT * FROM users WHERE id = $1", id)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (r *PostgresRepository) Create( ctx context.Context, user *User) error {
	log.Println("Create")
	log.Println("User: ", user)

	user.ID = uuid.NewV4().String()
	updateQuery := `
		UPDATE users SET (name, company, password) = ($1, $3, $4)
				WHERE email=$2;
	`
	if _, err := r.db.ExecContext(ctx, updateQuery, user.Name, user.Email, user.Company, user.Password); err != nil {
		return err
	}

	insertQuery := `
		INSERT INTO users (id, name, email, company, password) 
				SELECT $1, $2, CAST($3 AS VARCHAR), $4, $5
				WHERE NOT EXISTS (SELECT 1 FROM users WHERE email=$3);
	`
	_, err := r.db.ExecContext(ctx, insertQuery, user.ID, user.Name, user.Email, user.Company, user.Password)

	return err
}


func (r *PostgresRepository) GetByEmail(ctx context.Context, email string) (*pb.User, error) {
	log.Println("GetByEmail")

	user := &pb.User{}
	err := r.db.GetContext(ctx, user, "SELECT * FROM users WHERE email = $1", email)
	if err != nil {
		return nil, err
	}
	return user, nil
}

